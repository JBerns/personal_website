+++
# Display name
name = "Joris Berns"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "PhD Candidate in Management"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Tilburg University", url = "https://www.tilburguniversity.edu" } ]

# Short bio (displayed in user profile at end of posts)
bio = "My research interests include managerial decision-making, organizational failure, and Natural Language Processing."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "j.l.m.berns@tilburguniversity.edu"

# List (academic) interests or hobbies
interests = [
  "Managerial decision-making",
  "Corporate communication",
  "Organizational failure",
  "Natural Language Processing",
  "Machine Learning"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Researchers", "Visitors"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "PhD in Management"
  institution = "Tilburg University"
  year = "2022 (Expected)"

[[education.courses]]
  course = "MSc/MPhil in Business - Organization & Strategy (Cum Laude)"
  institution = "Tilburg University"
  year = 2018

[[education.courses]]
  course = "BSc in International Business Administration"
  institution = "Tilburg University"
  year = 2016
  
[[education.courses]]
  course = "BSc in Psychology"
  institution = "Tilburg University"
  year = 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/JorisBerns"

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/jorisberns"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "https://gitlab.com/JBerns"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
#[[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/BernsJoris_CV_2021.7.5.pdf"

+++
I am a PhD Candidate at the Management Department at Tilburg University. In my research I try to uncover how top managers behave when being faced with financial distress. The first major theme is trying to understand their decision-making through how reference points affect their risk-taking. A second theme focuses on their communication, examining how they verbally describe to external analysts how well their organization is doing. In my research I use a large variety of methods, ranging from advanced Natural Language Processing (NLP) techniques for analyzing textual and audio data, to using survival models. Furthermore, I am a strong believer in the Open Science movement and try to apply these principles in my own research. 

My supervisors are [Prof. Dr. Tal Simons](https://www.rsm.nl/people/tal-simons/) and [Prof. Dr. Zilin He](https://www.tilburguniversity.edu/staff/z-l-he).

I am on the academic job market as of summer 2021. If you like to know more about my work, please contact me via [email](mailto:j.l.m.berns@tilburguniversity.edu).
