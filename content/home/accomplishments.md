+++
# Accomplishments widget.
widget = "accomplishments"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Accomplish&shy;ments"
subtitle = ""

# Order that this section will appear in.
weight = 15

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  title = "Best Paper Nomination"
  organization = "AOM, STR Division"
  certificate_url = ""
  date_start = "2021-04-01"
  date_end = ""
  description = "Papers that receive this distinction belong to the top 10% of all submitted manuscripts in the STR division of the AOM 2021 conference."

[[item]]
  title = "Research grant"
  organization = "Google, Alphabet Inc."
  certificate_url = ""
  date_start = "2020-10-01"
  date_end = ""
  description = "Together with Prof. Dr. Stephan Hollander, we received funding from the ’Google Cloud Platform Research Credits Program’. In total, we received 5,000 dollars worth of credits, which we are able to allocate for research purposes."

[[item]]
  title = "Research grant"
  organization = "NWO, The Netherlands"
  certificate_url = ""
  date_start = "2019-12-01"
  date_end = ""
  description = "Together with Prof. Dr. Stephan Hollander, we received funding from the ’Computing Time on National Computer Facilities’ grant. This grant provides us with special access to the Dutch national supercomputer (Cartesius) for extensive big data applications."

[[item]]
  title = "Research grant"
  organization = "Google, Alphabet Inc."
  certificate_url = ""
  date_start = "2019-12-01"
  date_end = ""
  description = "Together with Prof. Dr. Stephan Hollander, we received funding from the ’Google Cloud Platform Research Credits Program’. In total, we received 10,000 dollars worth of credits, which we are able to allocate for research purposes."

[[item]]
  title = "CentER Scholarship"
  organization = "CentER, TiSEM, Tilburg University"
  certificate_url = ""
  date_start = "2017-09-01"
  date_end = ""
  description = "This scholarship is awarded to excellent students in the CentER program."
+++
