+++
# Custom widget - even more customized (credit to Clemens Fiedler)
widget = "activities"  # Do not modify this line!
active = true  # Activate this widget? true/false

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Teaching"

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Order that this section will appear in.
weight = 60

# Background (optional). 
#   Choose from a background color, gradient, or image.
#   Choose a dark or light text color, by setting `text_color_light`.
#   Delete or comment out (by prefixing `#`) any unused options.
[background]
  # Background color.
  # color = "navy"
  
  # Background gradient.
    # gradient_start = "SkyBlue"
    # gradient_end = "LightSkyBlue"
   
  
  # Background image.
  # image = "headers/bubbles-wide.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.1  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  # text_color_light = true

[[activity.teaching]]
  position = "BSc Internship Supervision" 
  institution = "Tilburg University" 
  year = "(2021 - Present)"

[[activity.teaching]]
  position = "Strategy for Pre-master" 
  institution = "Tilburg University" 
  year = "(2020 - 2021)"
  
  [[activity.teaching]]
  position = "BSc Thesis Supervision" 
  institution = "Tilburg University" 
  year = "(2020 - 2021)"
  
  [[activity.teaching]]
  position = "Projectmanagement IBA" 
  institution = "Tilburg University" 
  year = "(2019 - 2020)"

[[activity.teaching]]
  position = "Comparative & Cross-Cultural Management" 
  institution = "Tilburg University" 
  year = "(2019 - Present)"

[[activity.teaching]]
  position = "Creative Entrepreneurship" 
  institution = "Tilburg University" 
  year = "(2019 - 2020)"

[[activity.teaching]]
  position = "MSc Thesis Supervision" 
  institution = "Tilburg University" 
  year = "(2018 - 2021)"

[[activity.teaching]]
  position = "Organisatie & Strategie" 
  institution = "Tilburg University" 
  year = "(2018 - Present)"

+++
