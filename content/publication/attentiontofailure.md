+++

title = "Attention to reference point formation: Examining how attention affects reference dependence"

# Date first published.
date = "2021-7-31"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["J Berns", "T Simons", "Z He"] 

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = ""

# Abstract and optional shortened version.
abstract = "Reference points are fundamental to most behavioral decision-making. Prior scholarship has shown how reference points, e.g. aspiration levels, are not stable nor ever-present. We argue that these findings can be explained through *reference point formation*, the process of how reference points come into existence. From a cognitive-behavioral perspective, reference point formation needs to be understood through information processing. In this study, we measure changes in the distribution of attention, which are indicative of whether there are changes in decision-makers’ cognitive mechanisms. Using an innovative two-stage methodology, we first link how a specific shock in the information environment alters decision-makers’ allocation of attention across different loci. Second, we find that the relationship between reference points and risk-taking is dependent on whether there occurred a shift in the distribution of attention. Our main contribution is, through establishing the significance of reference point formation, that the existence of reference points is dependent on preceding cognitive processes. Another contribution is methodological, as our empirical approach and novel measurement techniques allow for a joint investigation of cognitive processes and reference dependence."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Is this a featured publication? (true/false)
featured = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Links (optional).
#url_pdf = "pdf/my-paper-name.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++

