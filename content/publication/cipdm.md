+++

title = "Developing theory on reference point formation: Bridging information processing and decision-making"
# Date first published.
# date = 2021-08-01T00:00:00
date = "2021-7-31"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["J Berns", "T Simons"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = ""

# Abstract and optional shortened version.
abstract = "Decision-making depends on processed information and information processing motivates the impending decision. Although these two processes are thus interdependent, scholars only rarely investigate information processing and decision-making jointly. This is problematic, as it has spurred individual streams of research that are increasingly divergent. We address this concern by establishing the Cognitive Information Processing and Decision-Making (CIPDM) model as an integrative framework of the two processes. In this model, we describe the key mechanisms that explain how a perceived information element is encoded in memory and subsequently utilized in the selection of an action. We contribute to the information process and decision-making literatures, establishing an integrative framework that paves the way for a joint investigation of information processing and decision-making. It highlights the importance of the mechanisms that link the two processes, opening up new avenues to investigation how a perceived element of information is able to affect the eventual selection of an action."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Is this a featured publication? (true/false)
featured = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Links (optional).
#url_pdf = "pdf/my-paper-name.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++
