+++

title = "Influencing security analysts through the strategic use of paraverbal communication: A multimodal big data approach"
# date = 2021-08-01T00:00:00
date = "2021-7-31"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["J Berns", "T Simons"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = ""

# Abstract and optional shortened version.
abstract = "Maintaining favorable analyst recommendations is important to organizations and their management, as these recommendations provide stakeholders with expert opinions about the organization’s well-being. Hence, managers experience a strong impetus to prevent analysts downgrading their organization. In verbal communication, such as in earnings calls, managers not only try to influence analysts via the words that they use, but also via the tone with which they speak (i.e. paraverbal communication). Paraverbal communication provides analysts with an extra channel of information, but is also strategic tool managers may use to exert influence. On the one hand management is inclined to offset negative information by talking positively, but on the other hand want to signal trustworthiness through congruent communication, by for example, using a negative tone when discussing negative topics. Leveraging several state-of-the-art Natural Language Processing algorithms, we analyze the incongruence between managers’ use of emotions in audio and textual data from earnings calls. Earnings calls are conference calls between the top management team and security analysts, offering an excellent context in which we can examine verbal communication between managers and analysts. We find that a balance between both paraverbal strategies characterize organizations that are least likely to get downgraded. Our study therefore contributes to the literature on corporate communication, demonstrating the importance of verbal tone in managing the relationship with market intermediaries (analysts). Furthermore, as this is one of the first studies in management that leverages audio data quantitatively, we demonstrate the theoretical and empirical need to consider the audio dimension when studying verbal communication."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Is this a featured publication? (true/false)
featured = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Links (optional).
#url_pdf = "pdf/my-paper-name.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++
