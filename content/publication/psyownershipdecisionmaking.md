+++

title = "Risky decision-making under the threat of organizational failure: Examining the role of formal versus psychological ownership"

# Date first published.
#date = 2021-08-01T00:00:00
date = "2021-7-31"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["J Berns", "T Simons"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = ""

# Abstract and optional shortened version.
abstract = "What does a Top-Management Team (TMT) member stand to lose when a strategy s/he has decided upon fails? We argue that whilst contemporary compensation packages provide some level of formal ownership to the TMT member, the TMT members' stake in the outcomes of their strategic decisions remain small. Extant research however finds TMT members as experiencing ownership over the organization and often making decisions as such. We attempt to reconcile the lack of formal ownership but experiencing it nonetheless by introducing the concept of *psychological ownership*, which is the feeling of ownership rather than having the rights over the object. Especially when the organization is at risk of being lost, organizational failure, we would expect to the differences between the two types of ownership to become salient. Using earnings calls, we employ Natural Language Processing (NLP) techniques to uncover how much ownership is expressed by TMT members, which we compare against any formal ownership they possess of their organization. Using a reference dependence perspective, when the organization is facing organizational failure, we expect the two types of ownership to affect risk-taking behavior differently."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Is this a featured publication? (true/false)
featured = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Links (optional).
#url_pdf = "pdf/my-paper-name.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++

