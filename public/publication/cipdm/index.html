<!DOCTYPE html>
<html lang="en-us">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Source Themes Academic 4.0.0">
  <meta name="generator" content="Hugo 0.55.6" />

  

  
  
  
  
  
    
    
    
  
  

  <meta name="author" content="Joris Berns">

  
  
  
    
  
  <meta name="description" content="Decision-making depends on processed information and information processing motivates the impending decision. Although these two processes are thus interdependent, scholars only rarely investigate information processing and decision-making jointly. This is problematic, as it has spurred individual streams of research that are increasingly divergent. We address this concern by establishing the Cognitive Information Processing and Decision-Making (CIPDM) model as an integrative framework of the two processes. In this model, we describe the key mechanisms that explain how a perceived information element is encoded in memory and subsequently utilized in the selection of an action. The CIPDM model is validated through a two-stage empirical approach, leveraging how the 2008 economic crisis affected U.S. commercial banks’ attention and subsequent risk-taking. We are thereby able to trace how perceived information elements ultimately affect decision-making. The merit of the CIPDM model is that it explains the process as a whole, as compared to the current single sub-mechanism models. We contribute to the information process and decision-making literatures, establishing an (quantitative) empirically-validated integrative framework that paves the way for a joint investigation of information processing and decision-making. It highlights the importance of the mechanisms that link the two processes, opening up new avenues to investigation how a perceived element of information is able to affect the eventual selection of an action.">

  
  <link rel="alternate" hreflang="en-us" href="/publication/cipdm/">

  


  

  

  

  

  

  

  
  
  
  <meta name="theme-color" content="#2962ff">
  

  
  
  
  
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha256-eSi1q2PG6J7g7ib17yAaWMcrr5GrtohYChqibrV7PBE=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/academicons/1.8.6/css/academicons.min.css" integrity="sha256-uFVgMKfistnJAfoCUQigIl+JfUaP47GrRKjf6CTPVmw=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" integrity="sha256-ygkqlh3CYSUri3LhQxzdcm0n1EQvH2Y+U5S2idbLtxs=" crossorigin="anonymous">

    
    
    
      
    
    
      
      
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css" crossorigin="anonymous" title="hl-light">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/dracula.min.css" crossorigin="anonymous" title="hl-dark" disabled>
        
      
    

    

    

  

  
  
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,400italic,700|Roboto+Mono">
  

  <link rel="stylesheet" href="/styles.css">
  

  
  
  

  
  <link rel="alternate" href="/index.xml" type="application/rss+xml" title="Joris Berns">
  <link rel="feed" href="/index.xml" type="application/rss+xml" title="Joris Berns">
  

  <link rel="manifest" href="/site.webmanifest">
  <link rel="icon" type="image/png" href="/img/icon.png">
  <link rel="apple-touch-icon" type="image/png" href="/img/icon-192.png">

  <link rel="canonical" href="/publication/cipdm/">

  
  
  
  
    
  
  <meta property="twitter:card" content="summary_large_image">
  
  <meta property="twitter:site" content="@JorisBerns">
  <meta property="twitter:creator" content="@JorisBerns">
  
  <meta property="og:site_name" content="Joris Berns">
  <meta property="og:url" content="/publication/cipdm/">
  <meta property="og:title" content="(Re-)establishing a unified framework of bounded rationality: A theoretical and empirical integration of information processing and decision-making | Joris Berns">
  <meta property="og:description" content="Decision-making depends on processed information and information processing motivates the impending decision. Although these two processes are thus interdependent, scholars only rarely investigate information processing and decision-making jointly. This is problematic, as it has spurred individual streams of research that are increasingly divergent. We address this concern by establishing the Cognitive Information Processing and Decision-Making (CIPDM) model as an integrative framework of the two processes. In this model, we describe the key mechanisms that explain how a perceived information element is encoded in memory and subsequently utilized in the selection of an action. The CIPDM model is validated through a two-stage empirical approach, leveraging how the 2008 economic crisis affected U.S. commercial banks’ attention and subsequent risk-taking. We are thereby able to trace how perceived information elements ultimately affect decision-making. The merit of the CIPDM model is that it explains the process as a whole, as compared to the current single sub-mechanism models. We contribute to the information process and decision-making literatures, establishing an (quantitative) empirically-validated integrative framework that paves the way for a joint investigation of information processing and decision-making. It highlights the importance of the mechanisms that link the two processes, opening up new avenues to investigation how a perceived element of information is able to affect the eventual selection of an action."><meta property="og:image" content="/img/headers/bubbles-wide.jpg">
  <meta property="og:locale" content="en-us">
  
  <meta property="article:published_time" content="2020-10-11T00:00:00&#43;00:00">
  
  <meta property="article:modified_time" content="2020-10-11T00:00:00&#43;00:00">
  

  

  

  <title>(Re-)establishing a unified framework of bounded rationality: A theoretical and empirical integration of information processing and decision-making | Joris Berns</title>

</head>
<body id="top" data-spy="scroll" data-target="#TableOfContents" data-offset="71" >
  <aside class="search-results" id="search">
  <div class="container">
    <section class="search-header">

      <div class="row no-gutters justify-content-between mb-3">
        <div class="col-6">
          <h1>Search</h1>
        </div>
        <div class="col-6 col-search-close">
          <a class="js-search" href="#"><i class="fas fa-times-circle text-muted" aria-hidden="true"></i></a>
        </div>
      </div>

      <div id="search-box">
        
        <input name="q" id="search-query" placeholder="Search..." autocapitalize="off"
        autocomplete="off" autocorrect="off" role="textbox" spellcheck="false" type="search">
        
      </div>

    </section>
    <section class="section-search-results">

      <div id="search-hits">
        
      </div>

    </section>
  </div>
</aside>


<nav class="navbar navbar-light fixed-top navbar-expand-lg py-0" id="navbar-main">
  <div class="container">

    
      <a class="navbar-brand" href="/">Joris Berns</a>
      
      <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span><i class="fas fa-bars"></i></span>
      </button>
      

    
    <div class="collapse navbar-collapse" id="navbar">

      
      
      <ul class="navbar-nav mr-auto">
        

        

        
        
        
          
        

        <li class="nav-item">
          <a class="nav-link" href="/#about">
            
            <span>Home</span>
            
          </a>
        </li>

        
        

        

        
        
        
          
        

        <li class="nav-item">
          <a class="nav-link" href="/#publications">
            
            <span>Publications</span>
            
          </a>
        </li>

        
        

        

        
        
        
          
        

        <li class="nav-item">
          <a class="nav-link" href="/files/2020.8.13_BernsJoris_CV.pdf">
            
            <span>CV</span>
            
          </a>
        </li>

        
        

        

        
        
        
          
        

        <li class="nav-item">
          <a class="nav-link" href="/#contact">
            
            <span>Contact</span>
            
          </a>
        </li>

        
        

      
      </ul>
      <ul class="navbar-nav ml-auto">
      

        

        
        <li class="nav-item">
          <a class="nav-link js-search" href="#"><i class="fas fa-search" aria-hidden="true"></i></a>
        </li>
        

        

        
        <li class="nav-item">
          <a class="nav-link js-dark-toggle" href="#"><i class="fas fa-moon" aria-hidden="true"></i></a>
        </li>
        

      </ul>

    </div>
  </div>
</nav>

<div class="pub" itemscope itemtype="http://schema.org/CreativeWork">

  









<div class="article-header">
  
  
  <img src="/img/headers/bubbles-wide.jpg" class="article-banner" itemprop="image" alt="">
  

  
</div>




  

  
  
  
<div class="article-container pt-3">
  <h1 itemprop="name">(Re-)establishing a unified framework of bounded rationality: A theoretical and empirical integration of information processing and decision-making</h1>

  

  
    



<meta content="2020-10-11 00:00:00 &#43;0000 UTC" itemprop="datePublished">
<meta content="2020-10-11 00:00:00 &#43;0000 UTC" itemprop="dateModified">

<div class="article-metadata">

  
  
  
  
  <div>
    



  <span itemscope itemprop="author" itemtype="http://schema.org/Person">
      <span itemprop="name">
        

      
      
      <a href="">J Berns</a></span></span>, 
  <span itemscope itemprop="author" itemtype="http://schema.org/Person">
      <span itemprop="name">
        

      
      
      <a href="">T Simons</a></span></span>
  



  </div>
  
  

  
  <span class="article-date">
    
    
      
    
    <time>October 2020</time>
  </span>
  

  

  

  
  

  

  
    
<div class="share-box" aria-hidden="true">
  <ul class="share">
    <li>
      <a class="twitter"
         href="https://twitter.com/intent/tweet?text=%28Re-%29establishing%20a%20unified%20framework%20of%20bounded%20rationality%3a%20A%20theoretical%20and%20empirical%20integration%20of%20information%20processing%20and%20decision-making&amp;url=%2fpublication%2fcipdm%2f"
         target="_blank" rel="noopener">
        <i class="fab fa-twitter"></i>
      </a>
    </li>
    <li>
      <a class="facebook"
         href="https://www.facebook.com/sharer.php?u=%2fpublication%2fcipdm%2f"
         target="_blank" rel="noopener">
        <i class="fab fa-facebook-f"></i>
      </a>
    </li>
    <li>
      <a class="linkedin"
         href="https://www.linkedin.com/shareArticle?mini=true&amp;url=%2fpublication%2fcipdm%2f&amp;title=%28Re-%29establishing%20a%20unified%20framework%20of%20bounded%20rationality%3a%20A%20theoretical%20and%20empirical%20integration%20of%20information%20processing%20and%20decision-making"
         target="_blank" rel="noopener">
        <i class="fab fa-linkedin-in"></i>
      </a>
    </li>
    <li>
      <a class="weibo"
         href="http://service.weibo.com/share/share.php?url=%2fpublication%2fcipdm%2f&amp;title=%28Re-%29establishing%20a%20unified%20framework%20of%20bounded%20rationality%3a%20A%20theoretical%20and%20empirical%20integration%20of%20information%20processing%20and%20decision-making"
         target="_blank" rel="noopener">
        <i class="fab fa-weibo"></i>
      </a>
    </li>
    <li>
      <a class="email"
         href="mailto:?subject=%28Re-%29establishing%20a%20unified%20framework%20of%20bounded%20rationality%3a%20A%20theoretical%20and%20empirical%20integration%20of%20information%20processing%20and%20decision-making&amp;body=%2fpublication%2fcipdm%2f">
        <i class="fas fa-envelope"></i>
      </a>
    </li>
  </ul>
</div>


  

</div>

    















  
</div>



  <div class="article-container">

    
    <h3>Abstract</h3>
    <p class="pub-abstract" itemprop="text">Decision-making depends on processed information and information processing motivates the impending decision. Although these two processes are thus interdependent, scholars only rarely investigate information processing and decision-making jointly. This is problematic, as it has spurred individual streams of research that are increasingly divergent. We address this concern by establishing the Cognitive Information Processing and Decision-Making (CIPDM) model as an integrative framework of the two processes. In this model, we describe the key mechanisms that explain how a perceived information element is encoded in memory and subsequently utilized in the selection of an action. The CIPDM model is validated through a two-stage empirical approach, leveraging how the 2008 economic crisis affected U.S. commercial banks’ attention and subsequent risk-taking. We are thereby able to trace how perceived information elements ultimately affect decision-making. The merit of the CIPDM model is that it explains the process as a whole, as compared to the current single sub-mechanism models. We contribute to the information process and decision-making literatures, establishing an (quantitative) empirically-validated integrative framework that paves the way for a joint investigation of information processing and decision-making. It highlights the importance of the mechanisms that link the two processes, opening up new avenues to investigation how a perceived element of information is able to affect the eventual selection of an action.</p>
    

    
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-12 col-md-3 pub-row-heading">Type</div>
          <div class="col-12 col-md-9">
            

            
            
            <a href="/publication/#3">
              Manuscript
            </a>
            
          </div>
        </div>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="d-md-none space-below"></div>
    

    

    <div class="space-below"></div>

    <div class="article-style"></div>

    

    






  
  
    
  
  








  </div>
</div>



<div class="container">
  <footer class="site-footer">
  

  <p class="powered-by">
    

    Powered by the
    <a href="https://sourcethemes.com/academic/" target="_blank" rel="noopener">Academic theme</a> for
    <a href="https://gohugo.io" target="_blank" rel="noopener">Hugo</a>.

    
    <span class="float-right" aria-hidden="true">
      <a href="#" id="back_to_top">
        <span class="button_icon">
          <i class="fas fa-chevron-up fa-2x"></i>
        </span>
      </a>
    </span>
    
  </p>
</footer>

</div>


<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <pre><code class="tex hljs"></code></pre>
      </div>
      <div class="modal-footer">
        <a class="btn btn-outline-primary my-1 js-copy-cite" href="#" target="_blank">
          <i class="fas fa-copy"></i> Copy
        </a>
        <a class="btn btn-outline-primary my-1 js-download-cite" href="#" target="_blank">
          <i class="fas fa-download"></i> Download
        </a>
        <div id="modal-error"></div>
      </div>
    </div>
  </div>
</div>

    

    
    
    
    <script src="/js/mathjax-config.js"></script>
    

    
    
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha512-+NqPlbbtM1QqiK8ZAo4Yrj2c4lNQoGv8P79DPtKzj++l5jnN39rHA/xsqn8zE9l0uSoxaCdrOgFs6yjyfbBxSg==" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js" integrity="sha256-lqvxZrPLtfffUl2G/e7szqSvPBILGbwmsGE1MKlOi0Q=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha256-VsEqElsCHSGmnmHXGQzvoWjWwoznFSZc6hs7ARLRacQ=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha256-CBrpuqrMhXwcLLUd5tvQ4euBHCdh7wGlDfNz8vbu/iI=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js" integrity="sha256-X5PoE3KU5l+JcX+w09p/wHl9AzK333C4hJ2I9S5mD4M=" crossorigin="anonymous"></script>

      
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js" integrity="sha256-/BfiIkHlHoVihZdc6TFuj7MmJ0TWcWsMXkeDFwhi0zw=" crossorigin="anonymous"></script>
        
      

      
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_CHTML-full" integrity="sha256-GhM+5JHb6QUzOQPXSJLEWP7R73CbkisjzK5Eyij4U9w=" crossorigin="anonymous" async></script>
      
    

    
    

    
    
    

    
    
    <script>hljs.initHighlightingOnLoad();</script>
    

    
    
    <script>
      const search_index_filename = "/index.json";
      const i18n = {
        'placeholder': "Search...",
        'results': "results found",
        'no_results': "No results found"
      };
      const content_type = {
        'post': "Posts",
        'project': "Projects",
        'publication' : "Publications",
        'talk' : "Talks"
        };
    </script>
    

    
    

    
    
    <script id="search-hit-fuse-template" type="text/x-template">
      <div class="search-hit" id="summary-{{key}}">
      <div class="search-hit-content">
        <div class="search-hit-name">
          <a href="{{relpermalink}}">{{title}}</a>
          <div class="article-metadata search-hit-type">{{type}}</div>
          <p class="search-hit-description">{{snippet}}</p>
        </div>
      </div>
      </div>
    </script>
    

    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fuse.js/3.2.1/fuse.min.js" integrity="sha256-VzgmKYmhsGNNN4Ph1kMW+BjoYJM2jV5i4IlFoeZA9XI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js" integrity="sha256-4HLtjeVgH0eIB3aZ9mLYF6E8oU5chNdjU6p6rrXpl9U=" crossorigin="anonymous"></script>
    

    
    

    
    
    
    
    
    
    
      
    
    
    
    
    <script src="/js/academic.min.109caa4f51ff39522c6dfc25fb05b6e9.js"></script>

  </body>
</html>

